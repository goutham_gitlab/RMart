//
//  Network.swift
//  RMart
//
//  Created by Goutham on 20/07/18.
//  Copyright © 2018 random. All rights reserved.
//

import Foundation

//MARK: - Network Manager Class

let HTTP_METHOD_POST = "POST"
let HTTP_METHOD_GET = "GET"

let Image_Download_URL = "https://media.redmart.com/newmedia/200p"
let ProductsURL = "https://api.redmart.com/v1.6.0/catalog/search?theme=all-sales&pageSize=30&page=0"

public struct Network {
    
    public func callAPI(url: String, http_Method: String, dict_Body: NSDictionary!, getParameters: NSString!, successCallBack: @escaping (_ dictResponse: NSDictionary) -> Void) {
        
        var dictResponseBack  =  Dictionary<String, Any>()
        
        let url_ = URL(string: url)
        
        //Appending JSON content
        var jsonData = Data()
        
        do {
            if let _ = dict_Body{
                let jsonResult = try JSONSerialization.data(withJSONObject: dict_Body, options: .prettyPrinted)
                jsonData = jsonResult
            }
            else{
//                print("dict_body is empty")
            }
        } catch let error as NSError {
            print("error in constructing dictionar: \(error.localizedDescription)")
        }
        
        
        var request = URLRequest(url: url_!)
        
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Accept")
        
        if http_Method == HTTP_METHOD_POST{
            
            if let _ = dict_Body{
                request.httpBody = jsonData
            }
            
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        }
        
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            
            if response != nil{
                
                let httpResponse = response as! HTTPURLResponse
                let statusCode = httpResponse.statusCode
                
                if (statusCode < 300) {
                    
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            
                            dictResponseBack = ["statusCode":"\(statusCode)", "statusMessage":"success", "data":json]
                            successCallBack(dictResponseBack as NSDictionary)
                            
                        } else if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray {
                            
                            // process "json" as an array
                            dictResponseBack = ["statusCode":"\(statusCode)", "statusMessage":"success", "data":json]
                            successCallBack(dictResponseBack as NSDictionary)
                            
                            print("\(json)")
                            
                        } else {
                            
                            dictResponseBack = ["statusCode":"\(statusCode)", "statusMessage":"success", "data":""]
                            successCallBack(dictResponseBack as NSDictionary)
                        }
                        
                    } catch {
                        print("error serializing JSON: \(error)")
                        dictResponseBack = ["statusCode":"\(statusCode)", "statusMessage":"fail", "data":"", "error": error]
                        successCallBack(dictResponseBack as NSDictionary)
                    }
                }
                else
                {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            
                            // process "json" as a dictionary
                            print("\(json)")
                            
                            dictResponseBack = ["statusCode":"\(statusCode)", "statusMessage":"fail", "data":json]
                            successCallBack(dictResponseBack as NSDictionary)
                            
                        } else if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray {
                            
                            // process "json" as an array
                            print("\(json)")
                            dictResponseBack = ["statusCode":"\(statusCode)", "statusMessage":"fail", "data":""]
                            successCallBack(dictResponseBack as NSDictionary)
                            
                        } else {
                            
                            print("Error could not parse JSON string:")
                            
                            dictResponseBack = ["statusCode":"\(statusCode)", "statusMessage":"fail", "data":""]
                            successCallBack(dictResponseBack as NSDictionary)
                        }
                        
                    } catch {
                        
                        dictResponseBack = ["statusCode":"\(statusCode)", "statusMessage":"fail", "data":""]
                        successCallBack(dictResponseBack as NSDictionary)
                        print("error serializing JSON: \(error)")
                        
                    }
                    
                    print("Login failed")
                }
            }
            else{
                
                dictResponseBack = ["statusCode":"\(0)", "statusMessage":"fail", "data":""]
                successCallBack(dictResponseBack as NSDictionary)
                print("empty response")
            }
            
        });
        
        // do whatever you need with the task e.g. run
        task.resume()
    }
}
