//
//  DetailedProduct.swift
//  RMart
//
//  Created by perk on 27/07/18.
//  Copyright © 2018 random. All rights reserved.
//

import Foundation
import Unbox

public struct DetailedProduct{
    let productItem: Product_
    static var product = DetailedProduct(productItem: Product_(id: "", desc: "", title: "", price: Pricing(price: ""), image: [ProductImage(name: "")]))
}

struct DetailedProduct_ {
    let id: String
    let desc: String
    let title: String
    let price: DetailedPricing
    let image: [DetailedProductImage]
}

extension DetailedProduct_: Unboxable {
    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.desc = try unboxer.unbox(key: "desc")
        self.title = try unboxer.unbox(key: "title")
        self.price = try unboxer.unbox(key: "pricing")
        self.image = try unboxer.unbox(key: "images")
    }
}

struct DetailedPricing {
    let price: String
}

extension DetailedPricing: Unboxable {
    init(unboxer: Unboxer) throws {
        self.price = try unboxer.unbox(key: "price")
    }
}

struct DetailedProductImage {
    let name: String
}

extension DetailedProductImage: Unboxable {
    init(unboxer: Unboxer) throws {
        self.name = try unboxer.unbox(key: "name")
    }
}
