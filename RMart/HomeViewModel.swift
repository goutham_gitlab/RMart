//
//  HomeViewModel.swift
//  RMart
//
//  Created by Goutham on 25/07/18.
//  Copyright © 2018 random. All rights reserved.
//

import UIKit
import Unbox

class HomeViewModel: HomeViewController {
    
    func fetchProductsDataFromServer(callBackProducts: (_ isDataFetchSuccess: Bool) -> Void)  {
        
        var isAPICallSuccess = Bool()
        
        let semaphore = DispatchSemaphore(value: 0)
        
        Network().callAPI(url: ProductsURL, http_Method: HTTP_METHOD_GET, dict_Body: nil, getParameters: nil,successCallBack:
            {(dictResponse: NSDictionary) -> Void in
                
                if dictResponse.value(forKey: "statusCode") as! String == "200" || dictResponse.value(forKey: "statusCode") as! String == "201"{
                    
                    do{
                        let dictDataOne = dictResponse.value(forKey: "data") as! NSDictionary
                        let products: [Product_] = try unbox(dictionary: dictDataOne as! UnboxableDictionary, atKeyPath: "products")
                        Product.product_list = Product(productItems: products as NSArray)

                        isAPICallSuccess = true
                        semaphore.signal()
                    }
                    catch{
                        print("Error in unboxing the session object")

                        isAPICallSuccess = false
                        semaphore.signal()
                    }
                }
                else{
                    print("Error respons in Property details after call back: \(dictResponse)")
                    
                    isAPICallSuccess = false
                    semaphore.signal()
                }
        })
        
        semaphore.wait()
        callBackProducts(isAPICallSuccess)
        
    }

    

}
