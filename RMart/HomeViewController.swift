//
//  HomeViewController.swift
//  RMart
//
//  Created by Goutham on 25/07/18.
//  Copyright © 2018 random. All rights reserved.
//

import UIKit
import Kingfisher
import PinterestLayout

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, PinterestLayoutDelegate {

    //MARK: - Properties
    @IBOutlet var collectionViewProducts: UICollectionView!
    
    //MARK: - ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //Seting up layout
        setupCollectionViewLayout()
        
        //Fetching Products from server
        fetchProducts()
        
    }
    
    //MARK: - CollectionView Delegate and Datasource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductViewCellIdentifier", for: indexPath) as! ProductViewCell
        
        let product = Product.product_list.productItems[indexPath.row] as! Product_
        let pricing: Pricing = product.price
        
        cell.labelPriceProduct.text = "$\(pricing.price)"
        cell.labelTitleProduct.text = product.title
        
        let imageURL = product.image[0] as ProductImage
        let url = URL(string: "\(Image_Download_URL)\(imageURL.name)")!

        cell.imageProduct.kf.setImage(with: url,
                                      placeholder: UIImage(named: "placeholder"),
                                      options: [.transition(.fade(1))],
                                      progressBlock: nil,
                                      completionHandler: {
                                        (image, error, cacheType, imageUrl) in
//                                        print("Error message: \(String(describing: error?.description))")
        })
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Product.product_list.productItems.count
    }
    
    func collectionView(collectionView: UICollectionView, heightForImageAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat {
        return 280
    }
    
    func collectionView(collectionView: UICollectionView, heightForAnnotationAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat {
        return 0
    }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Did select at index: \(indexPath.row)")
        
        let detailedProduct = Product.product_list.productItems[indexPath.row]
        DetailedProduct.product = DetailedProduct(productItem: detailedProduct as! Product_)
        
        
        performSegue(withIdentifier: "productToDetail", sender: nil)
        
    }
    
    //MARK: - Other Methods
    func setupCollectionViewLayout() {
        
        let layout = PinterestLayout()
        layout.delegate = self
        layout.cellPadding = 5
        layout.numberOfColumns = 3
        collectionViewProducts.collectionViewLayout = layout
        
        navigationController?.navigationBar.isHidden = true
    }
    
    func fetchProducts() {
        HomeViewModel().fetchProductsDataFromServer(callBackProducts: { isDataFetchSuccess in
            isDataFetchSuccess ? DispatchQueue.main.async { self.collectionViewProducts.reloadData() } : print("Failed to fetch/parse")
        })
    }
    
    //MARK: - Memory Warning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
