//
//  ProductViewCell.swift
//  RMart
//
//  Created by perk on 25/07/18.
//  Copyright © 2018 random. All rights reserved.
//

import UIKit

class ProductViewCell: UICollectionViewCell {
    
    @IBOutlet var imageProduct: UIImageView!
    @IBOutlet var labelTitleProduct: UILabel!
    @IBOutlet var labelPriceProduct: UILabel!
    
}
