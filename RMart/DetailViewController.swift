//
//  DetailViewController.swift
//  RMart
//
//  Created by Goutham Devaraju on 26/07/18.
//  Copyright © 2018 random. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, CarouselDelegate {

    //MARK: - Properties
    
    //Carousel view
    @IBOutlet var carouselView: Carousel!
    
    //Swipe down to dismiss
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelPrice: UILabel!
    @IBOutlet var labelDescription: UITextView!
    
    
    //MARK: - ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting carousel delegate
        carouselView.delegate = self
        
        configureDetailValues()
    }
    
    func configureDetailValues() {
        
        //Setting up information
        labelTitle.text = DetailedProduct.product.productItem.title
        labelDescription.text = DetailedProduct.product.productItem.desc
        
        let price = DetailedProduct.product.productItem.price
        labelPrice.text = price.price

        //Setting up images
        
        let imagesArray = NSMutableArray()
        let productImages: [ProductImage] = DetailedProduct.product.productItem.image
        for image in productImages{
            imagesArray.add(image.name)
        }
        
        carouselView.setCarouselData(paths: imagesArray as! [String],  describedTitle: [], isAutoScroll: true, timer: 5.0, defaultImage: "placeholder")
        carouselView.setCarouselOpaque(layer: false, describedTitle: false, pageIndicator: false)
        carouselView.setCarouselLayout(displayStyle: 0, pageIndicatorPositon: 2, pageIndicatorColor: nil, describedTitleColor: nil, layerColor: .clear)
        
    }
    
    //MARK: - Carousel DataSource & Delegates
    
    //require method
    func downloadImages(_ url: String, _ index:Int) {
        
//        let imageView = UIImageView()
//        imageView.kf.setImage(with: URL(string: url)!, placeholder: UIImage.init(named: "placeholder"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { (downloadImage, error, cacheType, url) in
//            
//            print("Error message: \(String(describing: error?.description))")
//            
//            self.carouselView.images[index] = downloadImage!
//        })
        
    }
    
    //optional method (interaction for touch image)
    func didSelectCarouselView(_ view:Carousel ,_ index:Int) {
        print("Image selected at index: \(index)")
    }
    
    //optional method (show first image faster during downloading of all images)
    func callBackFirstDisplayView(_ imageView: UIImageView, _ url: [String], _ index: Int) {
        imageView.kf.setImage(with: URL(string: url[index]), placeholder: UIImage.init(named: "placeholder"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
    }
    
    func startAutoScroll() {
        //optional method
        carouselView.startScrollImageView()
    }
    
    func stopAutoScroll() {
        //optional method
        carouselView.stopScrollImageView()
    }
    
    //MARK: - Pangesture Event
    @IBAction func panGestureEvent(_ sender: UIPanGestureRecognizer) {
        
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == UIGestureRecognizerState.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizerState.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizerState.ended || sender.state == UIGestureRecognizerState.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
    @IBAction func dismissDetailedView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    //MARK: - Memory Warning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
