//
//  Product.swift
//  RMart
//
//  Created by perk on 26/07/18.
//  Copyright © 2018 random. All rights reserved.
//

import Foundation
import Unbox

public struct Product{
    let productItems: NSArray
    static var product_list = Product(productItems: NSArray())
}

struct Product_ {
    let id: String
    let desc: String
    let title: String
    let price: Pricing
    let image: [ProductImage]
}

extension Product_: Unboxable {
    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.desc = try unboxer.unbox(key: "desc")
        self.title = try unboxer.unbox(key: "title")
        self.price = try unboxer.unbox(key: "pricing")
        self.image = try unboxer.unbox(key: "images")
    }
}

struct Pricing {
    let price: String
}

extension Pricing: Unboxable {
    init(unboxer: Unboxer) throws {
        self.price = try unboxer.unbox(key: "price")
    }
}

struct ProductImage {
    let name: String
}

extension ProductImage: Unboxable {
    init(unboxer: Unboxer) throws {
        self.name = try unboxer.unbox(key: "name")
    }
}



